/**
 * @ngdoc overview
 * @name f1App
 * @description Main app module.
 * # f1App
 */
angular
    .module('f1App', []).config(function ($sceDelegateProvider) {
        'use strict';
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain. **.
            'http://ergast.com/**'
        ]);
    });
