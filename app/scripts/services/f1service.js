/**
 * @ngdoc service
 * @name f1App.f1Service
 * @description Handles API calls to get f1 results information.
 * # f1Service
 * Factory in the f1App.
 */
angular.module('f1App')
    .factory('f1Service', function ($rootScope, $http) {
        'use strict';

        var f1Service = {};

        /**
         * Gets list of champions.
         * @returns {object} List of champions.
         */
        f1Service.getChampions = function () {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/driverStandings/1.json?limit=100'
            });
        };

        /**
         * Gets list of races for given year.
         * @param   {string} season Year to get races for.
         * @returns {object} List of races for year.
         */
        f1Service.getRaces = function (season) {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/' + season + '/results/1.json?limit=100'
            });
        };

        return f1Service;
    });
