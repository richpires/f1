/**
 * @ngdoc directive
 * @name f1App.directive:champions
 * @description List of champions.
 * # champions
 */
angular.module('f1App')
    .directive('champions', function ($rootScope) {
        'use strict';

        var controller = ['$rootScope', '$scope', 'f1Service', function ($rootScope, $scope, f1Service) {
            var vm = this;

            /**
             * Get selected champion.
             * @returns {object} Selected champion.
             */
            vm.getSelectedChampion = function () {
                return f1Service.selectedChampion;
            };

            /**
             * If clicked on champion, show all races in year.
             * @param {object} champion
             */
            vm.selectChampion = function (champion) {
                // Set or reset selected champion.
                if (f1Service.selectedChampion === champion) {
                    f1Service.selectedChampion = null;
                    $rootScope.$broadcast('showRaces', false);
                } else {
                    f1Service.selectedChampion = champion;
                    $rootScope.$broadcast('showRaces', true);
                }
            };

            /**
             * Filter for values within range.
             * @param   {string}  fieldName Field to filter on.
             * @param   {number}  minValue  Minimum range value.
             * @param   {number}  maxValue  Maximum range value.
             * @returns {boolean} Include value if true.
             */
            vm.filterByRange = function (fieldName, minValue, maxValue) {
                if (minValue === undefined || maxValue === undefined) {
                    return true;
                }

                return function predicateFunc(item) {
                    return minValue <= item[fieldName] && item[fieldName] <= maxValue;
                };
            };


            /**
             * Get list of champions from cache or service.
             */
            function init() {
                if (localStorage.champions) {
                    vm.champions = JSON.parse(localStorage.champions);
                    return;
                }

                f1Service.getChampions().then(function (result) {
                    vm.champions = result.data.MRData.StandingsTable.StandingsLists;

                    // Cache champions list.
                    localStorage.champions = JSON.stringify(vm.champions);
                });
            }

            init();
        }];

        function link(scope) {
            var champions = document.getElementsByClassName('champions')[0];

            // Show list of races after champions list animates.
            champions.addEventListener('transitionend', function (event) {
                if (event.propertyName === 'width' && scope.vm.getSelectedChampion()) {
                    $rootScope.$broadcast('showRaces', true);
                }
            }, false);
        }

        return {
            link: link,
            scope: true, // Make directive scope private.
            templateUrl: '../views/champions.html',
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            restrict: 'E'
        };
    });
