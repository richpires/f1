/**
 * @ngdoc directive
 * @name f1App.directive:races
 * @description List of races.
 * # races
 */
angular.module('f1App')
    .directive('races', function () {
        'use strict';

        var controller = ['$scope', 'f1Service', function ($scope, f1Service) {
            var vm = this;

            /**
             * Highlight race if winner was also year's champion.
             * @param   {object}  race
             * @returns {boolean} True if race should be highlighted.
             */
            vm.highlightRace = function (race) {
                if (f1Service.selectedChampion) {
                    if (race.Results[0].Driver.driverId === f1Service.selectedChampion.DriverStandings[0].Driver.driverId) {
                        return true;
                    }
                }

                return false;
            };

            /**
             * Get selected race season.
             * @returns {string} Selected race season.
             */
            vm.getRaceSeason = function () {
                return f1Service.selectedChampion && f1Service.selectedChampion.season;
            };

            // Show list of races for year.
            $scope.$on('showRaces', function (event, show) {
                vm.showRaces = show;
                vm.loading = true;

                if (vm.showRaces) {
                    // Clear list of races.
                    vm.races = null;

                    // Get list of races.
                    f1Service.getRaces(f1Service.selectedChampion.season).then(function (result) {
                        vm.races = result.data.MRData.RaceTable.Races;
                        vm.loading = false;
                    });
                }
            });
        }];

        return {
            scope: true, // Make directive scope private.
            templateUrl: '../views/races.html',
            controller: controller,
            controllerAs: 'vm',
            restrict: 'E'
        };
    });
