# f1

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `npm install` to install dependencies.
Run `grunt` for building and `grunt serve` for preview.

## Architecture

This application consists of two directives - champions and races: 
-Champions displays a list of champions for each year. 
-Races displays a list of races for a selected year. 

This application uses the Materialize UI framework.