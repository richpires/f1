'use strict';

describe('Directive: races', function () {

  // load the directive's module
  beforeEach(module('f1App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<races></races>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the races directive');
  }));
});
