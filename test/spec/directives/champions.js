'use strict';

describe('Directive: champions', function () {

  // load the directive's module
  beforeEach(module('f1App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<champions></champions>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the champions directive');
  }));
});
