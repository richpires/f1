'use strict';

describe('Service: f1Service', function () {

    // load the service's module
    beforeEach(module('f1App'));

    // instantiate service
    var f1Service;
    beforeEach(inject(function (_f1Service_) {
        f1Service = _f1Service_;
    }));

    it('should do something', function () {
        expect(!!f1Service).toBe(true);
    });

});
